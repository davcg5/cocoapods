//
//  PokemonTableViewCell.swift
//  AppPokemon
//
//  Created by Laboratorio FIS on 3/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillData(pokemon:Pokemon){
        nameLabel.text = pokemon.name
        heightLabel.text = "\(pokemon.height ?? 0)"
        weightLabel.text = "\(pokemon.weight ?? 0)"
    }
    
}
