//
//  Pokemons.swift
//  AppPokemon
//
//  Created by Laboratorio FIS on 2/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//


import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage


protocol PokemonServiceDelegate{
    func get20FirstPokemons(pokemons:[Pokemon])
}

class PokemonService{
    
    var delegate:PokemonServiceDelegate?
    
    
    func downloadPokemons(){
        
        var pokemonArray: [Pokemon] = []
        let dpGR = DispatchGroup()
        for i in 1...20{
            dpGR.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
                
                let pokemon = response.result.value
                pokemonArray.append(pokemon!)
                //print("pokemon \(i)")
                dpGR.leave()
                
            }
            
        }
        dpGR.notify(queue: .main){
            pokemonArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
            self.delegate?.get20FirstPokemons(pokemons: pokemonArray)
        }
        
        
    }
    
    func getPokemonImage(id:Int, completion:@escaping(UIImage)->()){
        let URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        Alamofire.request(URL).responseImage{
            response in
            if let image = response.result.value{
                completion(image)
            }
        }
    }

}

