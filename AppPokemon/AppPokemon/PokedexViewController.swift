//
//  PokedexViewController.swift
//  AppPokemon
//
//  Created by Laboratorio FIS on 29/11/17.
//  Copyright © 2017 DAVID. All rights reserved.
//
import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {
    
    @IBOutlet weak var pokedexTable: UITableView!
    
    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let service = PokemonService()
        service.delegate = self
        service.downloadPokemons()
    }
    
    func get20FirstPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTable.reloadData()
    }
    
    //MARK:- TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //numero de secciones
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //numero de filas en cada seccion
        return pokemonArray.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //devuelve la data de cada fila
        let cell = tableView.dequeueReusableCell(withIdentifier: "PokemonCell") as! PokemonTableViewCell
        //cell.textLabel?.text = "\(pokemonArray[indexPath.row].id)" //+ pokemonArray[indexPath.row].name
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Pokemon"
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetail = segue.destination as! DataViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    }
    


}
