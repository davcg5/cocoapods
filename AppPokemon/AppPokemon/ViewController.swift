//
//  ViewController.swift
//  AppPokemon
//
//  Created by Laboratorio FIS on 29/11/17.
//  Copyright © 2017 DAVID. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
class ViewController: UIViewController {

    
    @IBOutlet weak var nombreLabel1: UILabel!
    
    
    @IBOutlet weak var pesoLabel: UILabel!
    @IBOutlet weak var alturaLabel: UILabel!
    @IBOutlet weak var descriocionTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func consultarButton(_ sender: Any) {
    
    let pkID = arc4random_uniform(250)+1
        let URL = "https://pokeapi.co/api/v2/pokemon/\(pkID)"
        Alamofire.request(URL).responseObject{(response: DataResponse<Pokemon>)in
            let pokemon = response.result.value
           
            DispatchQueue.main.async {
                self.nombreLabel1.text = pokemon?.name
                self.alturaLabel.text = "\(pokemon?.height ?? 0)"
                self.pesoLabel.text = "\(pokemon?.weight ?? 0)"
                
            }
            
        }
    }
    
    
}

