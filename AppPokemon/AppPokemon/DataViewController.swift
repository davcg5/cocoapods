//
//  DataViewController.swift
//  AppPokemon
//
//  Created by Laboratorio FIS on 3/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

    
    @IBOutlet weak var pokemonView: UIImageView!
    var pokemon: Pokemon?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = pokemon?.name
        let pkService = PokemonService()
        pkService.getPokemonImage(id: (pokemon?.id)!) {
            (pkImage) in
            self.pokemonView.image = pkImage
        }
    }


}
